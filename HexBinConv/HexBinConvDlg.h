
// HexBinConvDlg.h : header file
//

#pragma once


// CHexBinConvDlg dialog
class CHexBinConvDlg : public CDialogEx
{
// Construction
public:
	CHexBinConvDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_HEXBINCONV_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnBin2hex();
	afx_msg void OnBnClickedBtnHex2bin();

	CFont m_FontBtn;
};

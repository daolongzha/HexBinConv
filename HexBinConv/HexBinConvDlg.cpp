
// HexBinConvDlg.cpp : implementation file
//

#include "stdafx.h"
#include "HexBinConv.h"
#include "HexBinConvDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CHexBinConvDlg dialog




CHexBinConvDlg::CHexBinConvDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CHexBinConvDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CHexBinConvDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CHexBinConvDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_BIN2HEX, &CHexBinConvDlg::OnBnClickedBtnBin2hex)
	ON_BN_CLICKED(IDC_BTN_HEX2BIN, &CHexBinConvDlg::OnBnClickedBtnHex2bin)
END_MESSAGE_MAP()


// CHexBinConvDlg message handlers

BOOL CHexBinConvDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CButton* pBtn;
	m_FontBtn.CreatePointFont(142, _T("Calibri"));
	pBtn= (CButton*)GetDlgItem(IDC_BTN_BIN2HEX);
	pBtn->SetFont(&m_FontBtn);
	pBtn = (CButton*)GetDlgItem(IDC_BTN_HEX2BIN);
	pBtn->SetFont(&m_FontBtn);


	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CHexBinConvDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CHexBinConvDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CHexBinConvDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CHexBinConvDlg::OnBnClickedBtnBin2hex()
{
	// TODO: Add your control notification handler code here
	//IDC_BTN_BIN2HEX
	CString strFilter = _T("二进制文件(*.bin)|*.bin|");
	CString strBinFile;
	CFile file;
	UINT nFileLen;
	CString strFile;
	CString strTextFile;
	CStdioFile stdFile;
	CString str;
	UINT nOffset;

	CFileDialog dlg(TRUE, _T("txt"), NULL, 0, strFilter, this);

	if(dlg.DoModal() == IDOK)
	{
		strBinFile = dlg.GetPathName();
		strTextFile = strBinFile.Left(strBinFile.GetLength()-3)+_T("txt");
#ifdef _UNICODE
		if(_waccess(strTextFile,0) == 0) // if existed
#else
		if(access(strTextFile,0) == 0)
#endif
		{
			if(MessageBox(_T("文件已存在，是否替换？\n")+strTextFile,_T("提示"),MB_OKCANCEL) != IDOK)
				return;
		}
		// 执行以二进制打开文件
		if(!file.Open(strBinFile, CFile::modeRead|CFile::typeBinary))
		{
			MessageBox(_T("打开文件失败！\n")+strBinFile);
			return;
		}
		nFileLen = (UINT)file.GetLength();
		unsigned char *pBuf = new unsigned char[(UINT)(nFileLen+1)];
		file.Read(pBuf, nFileLen);
		strFile.Empty();
		for(UINT i=0; i<nFileLen; i++)
		{
			strFile.AppendFormat(_T("%02X"), pBuf[i]);
		}
		delete[]pBuf;
		file.Close();

		if(!stdFile.Open(strTextFile,CFile::modeCreate|CFile::modeWrite))
		{
			MessageBox(_T("创建文件失败!\n")+strTextFile);
			return;
		}
		nOffset = 0;
		while (nOffset < nFileLen*2)
		{
			str.Empty();
			for(int i=0; i<32; i+=2)
			{
				str.AppendFormat(_T("%s "),strFile.Mid(nOffset+i,2));
			}
			nOffset += 32;
			stdFile.WriteString(str+_T("\n"));
		}
		stdFile.Close();
		MessageBox(_T("转换完成！"));
	}
}


void CHexBinConvDlg::OnBnClickedBtnHex2bin()
{
	// TODO: Add your control notification handler code here
	//IDC_BTN_HEX2BIN
	CString strFilter = _T("文本文件(*.txt)|*.txt|");
	CString strTextFile;
	CFile file;
	UINT nFileLen;
	CString strFile;
	CString strBinFile;
	CString str;
	UINT nOffset;

	CFileDialog dlg(TRUE, _T("txt"), NULL, 0, strFilter, this);

	if(dlg.DoModal() == IDOK)
	{
		strTextFile = dlg.GetPathName();
		strBinFile = strTextFile.Left(strTextFile.GetLength()-3)+_T("bin");
#ifdef _UNICODE
		if(_waccess(strBinFile,0) == 0) // if existed
#else
		if(access(strBinFile,0) == 0)
#endif
		{
			if(MessageBox(_T("文件已存在，是否替换？\n")+strBinFile,_T("提示"),MB_OKCANCEL) != IDOK)
				return;
		}
		// 执行以二进制打开文件
		if(!file.Open(strTextFile, CFile::modeRead))
		{
			MessageBox(_T("打开文件失败！\n")+strTextFile);
			return;
		}
		nFileLen = (UINT)file.GetLength();
		unsigned char *pBuf = new unsigned char[(UINT)(nFileLen+1)];
		file.Read(pBuf, nFileLen);
		pBuf[nFileLen] = 0;
		strFile = pBuf;
		delete[]pBuf;
		file.Close();

		strFile.TrimLeft();
		strFile.TrimRight();
		strFile.Replace(_T(" "),_T(""));
		strFile.Replace(_T("0x"),_T(""));
		strFile.Replace(_T("0X"),_T(""));
		strFile.Replace(_T("\r"),_T(""));
		strFile.Replace(_T("\n"),_T(""));
		strFile.Replace(_T("\t"),_T(""));

		nOffset = 0;
		nFileLen = strFile.GetLength();
		unsigned char* pDes = new unsigned char[nFileLen/2+1];
		while(nOffset<nFileLen)
		{
#ifdef _UNICODE
			if(!iswxdigit(strFile.GetAt(nOffset)) || !iswxdigit(strFile.GetAt(nOffset+1)))
#else
			if(!isxdigit(strFile.GetAt(nOffset)) || !isxdigit(strFile.GetAt(nOffset+1)))
#endif
			{
				MessageBox(_T("文本中有非十六进制字符！\n")+strFile.Mid(nOffset,2));
				return;
			}
#ifdef _UNICODE
			swscanf_s(strFile.Mid(nOffset,2),_T("%2hhX"),(unsigned char *)pDes+nOffset/2);
#else
			sscanf_s(strFile.Mid(nOffset,2),_T("%2hhX"),(unsigned char *)pDes+nOffset/2);
#endif
			nOffset += 2;
		}
		if(!file.Open(strBinFile,CFile::modeCreate|CFile::modeWrite))
		{
			MessageBox(_T("打开文件失败！\n")+strBinFile);
			return;
		}
		file.Write(pDes,nFileLen/2);
		file.Close();

		MessageBox(_T("转换完成！"));
	}
}

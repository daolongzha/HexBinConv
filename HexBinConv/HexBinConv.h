
// HexBinConv.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CHexBinConvApp:
// See HexBinConv.cpp for the implementation of this class
//

class CHexBinConvApp : public CWinApp
{
public:
	CHexBinConvApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CHexBinConvApp theApp;